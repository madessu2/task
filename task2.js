/* 
Buat fungsi untuk menghitung luas dan keliling lingkaran
*/
function menghitungLingkaran (r) {
    const pi = 3.14;
    const rumusLuas = 2 * pi * r;
    const rumusKeliling = pi * r * r;
    return {rumusKeliling, rumusLuas};
};
const {rumusKeliling, rumusLuas} = menghitungLingkaran(4)
console.log(rumusKeliling);
console.log(rumusLuas);